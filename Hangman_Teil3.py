# example for Hangman
import sys
from random import randint

Lst = ['administration', 'beautiful', 'commercial', 'development', 'education', 'family', 'generation', 'hospital',
       'information', 'job', 'knowledge', 'language', 'management', 'newspaper', 'organization', 'performance',
       'question', 'responsibility', 'scientist', 'technology', 'understand', 'various', 'window', 'yourself']


class Character():
    def __init__(self, char, occupy):
        self.char = char
        self.occupy = occupy

    def check_the_char(self, char_to_check):
        if char_to_check == self.char:
            self.occupy = True
            return True
        else:
            return False


def show_the_word(the_word):
    print(" ")
    for k in range(0, len(the_word)):
        if the_word[k].occupy:
            sys.stdout.write(f"| {the_word[k].char} ")
        else:
            sys.stdout.write("| - ")
    sys.stdout.write("|")


def check_the_win(the_word):
    number_of_opened = 0
    for p in range(0, len(the_word)):
        if the_word[p].occupy:
            number_of_opened += 1
        else:
            pass
    if number_of_opened == len(the_word):
        return True
    else:
        return False


def get_char():
    input_char = "None"
    allowed_chars = 'abcdefghijklmnopqrstuvwxyz'
    while len(input_char) != 1 or input_char not in allowed_chars:
        input_char = input("\nEnter a character: ")
        input_char = input_char.lower()
        if input_char.isdigit():
            print("You entered a number!")
        elif input_char == '+':
            raise KeyboardInterrupt
        elif input_char not in allowed_chars:
            print("That character is not allowed!")
        else:
            pass
    return input_char


def play(random):
    word = Lst[random]
    str_in_char = []
    attempts = 10
    won = False

    for i in range(0, len(word)):
        str_in_char.append(Character(word[i], False))

    while not won and attempts != 0:
        char = get_char()

        for i in range(0, len(str_in_char)):
            if str_in_char[i].check_the_char(char):
                attempts += 1
            else:
                pass

        show_the_word(str_in_char)
        won = check_the_win(str_in_char)
        attempts -= 1
        print(f"\nleftover attempts: {attempts}")

    if won:
        print("\n\nCongratulations! You guessed the word!\n")
        if 6 <= attempts <= 10:
            print(f"\nYou used only {10 - attempts} attempts\n"
                  "Good play! :)")
        elif 1 <= attempts <= 5:
            print(f"\nYou used {10 - attempts} attempts\n"
                  "\nCould be better! :|")
        else:
            print("\nYou used all your attempts!\n"
                  "You lost! :(")
    else:
        print("You lost! :(")


def menu():
    new = False
    print("\n--------------------Hangman--------------------\n"
          "-------------Conditions for a win:-------------\n"
          "You have 10 attempts\n"
          "+1 attempt if you open a double char\n"
          "A win with 1-6 attempts leftover: 10 points\n"
          "A win with 7-10 attempts leftover: 5 points\n"
          "Lost: 0 points\n"
          "Exit: press '+'\n"
          "--------------------- Good luck!---------------------")

    try:
        play(randint(0, len(Lst)))
    except KeyboardInterrupt:
        print("Cancelling the game")

    print('\n\nNew game: Y\n'
          'Close: N\n')

    while True:
        close_the_while = False
        input_char = input("> ")
        input_char = input_char.lower()
        if input_char == 'y':
            new = True
            close_the_while = True
        elif input_char == 'n':
            close_the_while = True
        else:
            pass
        if close_the_while:
            break
    return new


def main():
    while True:
        new_game = menu()

        if not new_game:
            break


main()

