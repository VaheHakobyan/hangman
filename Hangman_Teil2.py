# example for Hangman
import sys
from random import randint

Lst = ['administration', 'beautiful', 'commercial', 'development', 'education', 'family', 'generation', 'hospital',
       'information', 'job', 'knowledge', 'language', 'management', 'newspaper', 'organization', 'performance',
       'question', 'responsibility', 'scientist', 'technology', 'understand', 'various', 'window', 'yourself']


def check_the_char(the_word, char_to_check, char_positions):
    for i in range(0, len(the_word)):
        if char_to_check == the_word[i]:
            char_positions.append(i)
        else:
            pass


def show_the_feedback(the_word, occupied_positions, attempts):
    for k in range(0, len(the_word)):
        open_the_char = False
        for z in range(0, len(occupied_positions)):
            if k == occupied_positions[z]:
                sys.stdout.write(f"| {the_word[k]} ")
                open_the_char = True
        if not open_the_char:
            sys.stdout.write("| - ")
        else:
            pass
    sys.stdout.write("|\n")
    print(f"leftover attempts: {attempts}")


def check_the_win(the_word, occupied_positions):
    if len(the_word) == len(occupied_positions):
        return True
    else:
        return False


def get_char(word, occupied_positions):
    input_char = "None"
    allowed_chars = 'abcdefghijklmnopqrstuvwxyz'
    while len(input_char) != 1 or input_char not in allowed_chars or already_opened:
        already_opened = False
        input_char = input("\nEnter a character: ")
        input_char = input_char.lower()
        for z in range(0, len(occupied_positions)):
            if input_char == word[occupied_positions[z]]:
                print("That character is already opened!")
                already_opened = True
        if input_char.isdigit():
            print("You entered a number!")
        elif input_char == '+':
            raise KeyboardInterrupt
        elif input_char not in allowed_chars:
            print("That character is not allowed!")
        else:
            pass
    return input_char


def play(random_word):
    word = Lst[random_word]
    str_in_char = []
    occupied_positions = []
    number_of_occupied_positions = 0
    won = False
    attempts = 10

    for i in range(0, len(word)):
        str_in_char.append(word[i])

    while not won and attempts != 0:
        char = get_char(str_in_char, occupied_positions)
        check_the_char(str_in_char, char, occupied_positions)
        if not len(occupied_positions) - number_of_occupied_positions:
            attempts -= 1
        else:
            pass
        show_the_feedback(str_in_char, occupied_positions, attempts)
        won = check_the_win(str_in_char, occupied_positions)
        number_of_occupied_positions = len(occupied_positions)
    if won:
        print("\n\nCongratulations! You guessed the word!\n")
        print(f"It is the word: {word}")
        if 6 <= attempts <= 10:
            print(f"\nYou used only {10-attempts} attempts\n"
                  "Good play! :)")
        elif 1 <= attempts <= 5:
            print(f"\nYou used {10-attempts} attempts\n"
                  "\nCould be better! :|")
        else:
            print("\nYou used all your attempts!\n"
                  "You lost! :(")
    else:
        print("You lost! :(")


def menu():
    new = False
    print("\n--------------------Hangman--------------------\n"
          "-------------Conditions for a win:-------------\n"
          "You have 10 attempts\n"
          "+1 attempt if you open a double char\n"
          "A win with 1-6 attempts leftover: 10 points\n"
          "A win with 7-10 attempts leftover: 5 points\n"
          "Lost: 0 points\n"
          "Exit: press '+'\n"
          "--------------------- Good luck!---------------------")

    try:
        play(randint(0, len(Lst)))
    except KeyboardInterrupt:
        print("Cancelling the game")

    print('\nNew game: Y\n'
          'Close: N\n')

    while True:
        close_the_while = False
        input_char = input("> ")
        input_char = input_char.lower()
        if input_char == 'y':
            new = True
            close_the_while = True
        elif input_char == 'n':
            close_the_while = True
        else:
            pass
        if close_the_while:
            break
    return new


def main():
    while True:
        new_game = menu()

        if not new_game:
            break


main()
